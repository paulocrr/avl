#include <iostream>
#include "cAvlTree.h"
using namespace std;
int main() {
    cAvlTrees<int> mytree(1);
    mytree.Insert(8);
    mytree.Insert(3);
    mytree.Insert(7);
    mytree.Insert(2);
    mytree.Insert(9);
    mytree.Insert(4);
    mytree.Insert(5);
    mytree.Insert(6);
    mytree.Remove(2);
    mytree.Remove(1);
    mytree.Remove(6);
    mytree.Remove(7);
    mytree.Remove(9);
    mytree.Remove(3);
    mytree.Remove(4);
    mytree.imprimir();
    return 0;
}